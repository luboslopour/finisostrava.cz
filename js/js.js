/* WWW.???.CZ, JS LIBRARY */
function checkLength(o, n, min, max) {
    //alert(o.length)
    if (!o.length || o.val().length > max || o.val().length < min) {
        //alert('fault');
        n.addClass('fault');
        return false;
    } else {
        //alert('succes');
        return true;
    }
}

function checkRegexp(o, regexp, n) {
    if (!(regexp.test(o.val()))) {
        n.addClass('fault');
        return false;
    } else {
        return true;
    }
}

jQuery(document).ready(function($) {

    // Open links in new window
    $("a[rel='external']").click(function() {
        window.open(this.href);
        return false;
    });

    // Focus text for search
    $("#searchstring, #intranet").focus(function() {
        if ($(this).val() == $(this)[0].title) {
            $(this).removeClass("TextActive");
            $(this).val("");
        }
    });
    $("#searchstring, #intranet").blur(function() {
        if ($(this).val() == "") {
            $(this).addClass("TextActive");
            $(this).val($(this)[0].title);
        }
    });
    $(".Text").blur();

    // Antispam
    if ($('.antispamblock').length) {
        $('.antispam').val('777');
        $('.antispamblock').hide();
    }

    // doplnuje grafiku blockquote
    $('div.content blockquote').append('<span class="ico"></span>');

    // hezke selecty/inputy ve formularich
    if ($('form').length) {
        $('form select').sSelect();
        $('form div.newListSelected').append('<div class="dropdown"><span class="ico"></span></div>');
    }

    $('div.dropdown').click(function() {
        $(this).parents('div.newListSelected').find('div.SSContainerDivWrapper').toggle();
    });

    $('div.newListSelected').click(function() {
        if ($(this).find('div.selectedTxt').text() != 'Vyberte jednu z možností') {
            $(this).css('color', 'rgb(0,0,0)');
        } else {
            $(this).css('color', 'rgb(102,102,102)');
        }
    });


    $('form input[type=text], form textarea').click(function() {
        if ($(this).attr("title") != undefined)
        {
            if ($(this).val() == $(this).attr("title"))
                $(this).val('');
        }
    });
    $('form input[type=text], form textarea').blur(function() {
        if ($(this).attr("title") != undefined)
        {
            if ($(this).val() == '')
                $(this).val($(this).attr("title"));
        }
    });

    // obsluhuje textova pole/textarey pro default text
    $('form input[type=text], form textarea').click(function() {
        if ($(this).attr("title") != undefined)
        {
            if ($(this).val() == $(this).attr("title"))
                $(this).val('');
        }
    });
    $('form input[type=text], form textarea').blur(function() {
        if ($(this).attr("title") != undefined)
        {
            if ($(this).val() == '')
                $(this).val($(this).attr("title"));
        }
    });

    // obsluhuje galerii v detailu clanku
    $('div.gallery a').each(function(index) {
        $(this).attr('rel', 'gallery');
    });

    $('div.gallery a').fancybox({
        padding: 20,
        overlayOpacity: 0.75,
        overlayColor: 'rgb(0,0,0)',
        titlePosition: 'inside',
        titleFormat: function(title, currentArray, currentIndex, currentOpts) {
            return '<samp>' + (currentIndex + 1) + ' / ' + currentArray.length + '</samp> ' + title;
        }
    });

    // obsluhuje fancybox
    $('a#fancybox-close').append('<span class="ico"></span>');
    $('a#fancybox-left').attr('title', 'Předchozí');
    $('a#fancybox-right').attr('title', 'Další');
    $('a#fancybox-close').attr('title', 'Zavřít');

    // obsluhuje/spousti hlavni slider na homepage
    if ($('div.homepage div.content div.main.slider').length) {
        var homepage_slider = $('div.content div.main.slider div.items').bxSlider({
            mode: 'horizontal',
            slideSelector: '',
            infiniteLoop: true,
            hideControlOnEnd: false,
            speed: 1500,
            easing: null,
            slideMargin: 0,
            startSlide: 0,
            randomStart: false,
            captions: false,
            ticker: false,
            tickerHover: false,
            adaptiveHeight: false,
            adaptiveHeightSpeed: 500,
            video: false,
            useCSS: true,
            preloadImages: 'all',
            touchEnabled: true,
            swipeThreshold: 50,
            oneToOneTouch: true,
            preventDefaultSwipeX: true,
            preventDefaultSwipeY: false,
            pager: false,
            pagerType: 'full',
            pagerShortSeparator: ' / ',
            pagerSelector: null,
            buildPager: null,
            pagerCustom: null,
            controls: true,
            nextText: '&rarr;<span class="ico"></span>',
            prevText: '&larr;<span class="ico"></span>',
            nextSelector: null,
            prevSelector: null,
            autoControls: false,
            startText: 'Start',
            stopText: 'Stop',
            autoControlsCombine: false,
            autoControlsSelector: null,
            auto: true,
            pause: 8000,
            autoStart: true,
            autoDirection: 'next',
            autoHover: false,
            autoDelay: 0,
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            slideWidth: 0
        });
        $('a.bx-next').attr('title', 'Další');
        $('a.bx-prev').attr('title', 'Předchozí');
    }

    // obsluhuje vyber data
    if ($('input.datepicker').length) {
        $('input.datepicker').datepicker({
            closeText: 'Zavřít',
            prevText: '',
            nextText: '',
            currentText: 'Dnes',
            monthNames: ['Leden', 'Únor', 'Březen', 'Duben', 'Květen', 'Červen', 'Červenec', 'Srpen', 'Září', 'Říjen', 'Listopad', 'Prosinec'],
            monthNamesShort: ['Le', 'Ún', 'Bř', 'Du', 'Kv', 'Čn', 'Čc', 'Sr', 'Zá', 'Ří', 'Li', 'Pr'],
            dayNames: ['Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota'],
            dayNamesShort: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
            dayNamesMin: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
            weekHeader: 'Sm',
            dateFormat: "dd. mm. yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            minDate: 0,
            showButtonPanel: false
        });
    }

    // nastavuje/obsluhuje modaly
    $('#mail, #searchform, #we-call-back').dialog({
        autoOpen: false,
        height: 'auto',
        width: 500,
        modal: true
    });

    $('li.mail a').click(function() {
        $('#mail').dialog('open');
        return false;
    });

    $('#submitFriend').click(function() {
        //alert('test');
        var email1 = $('#mail-id1');
        var email2 = $('#mail-id2');
        var label_email1 = $('#label_mail-id1').parent();
        var label_email2 = $('#label_mail-id2').parent();
        var allFields = $([]).add(label_email1).add(label_email2);
        var bValid = true;
        //alert(bValid);
        allFields.removeClass("fault");
        //alert(bValid);
        bValid = checkLength(email1, label_email1, 3, 80) && bValid;
        //alert(bValid);
        bValid = checkLength(email2, label_email2, 3, 80) && bValid;
        //alert(bValid);

        bValid = checkRegexp(email1, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, label_email1) && bValid;
        //alert(bValid);
        bValid = checkRegexp(email2, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, label_email2) && bValid;
        //alert(bValid);

        if (!bValid) {
            $('#send-to-friend .message').hide();
            $('#send-to-friend .message.fault').show();
            return false;
        } else {
            $('#send-to-friend .message').hide();
            $('#send-to-friend .message.success').show();
        }

    });


    $('li.search a').click(function() {
        $('#searchform').dialog('open');
        return false;
    });
    $('#submitSearchForm').click(function() {
        //alert('test');
        var searchS = $('#searchstring');
        var label_search = $('#label_searchstring').parent();

        var searchControl = $('#searchstringControl').val();

        var allFields = $([]).add(label_search);
        var bValid = true;
        var searchValid = true;
        //alert(bValid);
        allFields.removeClass("fault");
        //alert(bValid);
        searchValid = checkLength(searchS, label_search, 2, 80);
        bValid = checkLength(searchS, label_search, 2, 80) && bValid;
        //alert(bValid);
        //alert(searchControl+' = '+searchS.val());
        if (searchControl === searchS.val()) {
            bValid = false;
            label_search.addClass("fault");
        } else if (!searchValid) {
            searchS.val(searchControl);
        }

        //alert(bValid);

        if (!bValid) {
            $('#searchform .message').hide();
            $('#searchform .message.fault').show();
            return false;
        } else {
            $('#searchform .message').hide();
            $('#searchform .message.success').show();
        }

    });


    $('p.we-call-back a').click(function() {
        $('#we-call-back').dialog('open');
        return false;
    });
    $('#submitCallback').click(function() {
        //alert('test');
        var phone = $('#callback-id1');
        var term = $('#callback-id2');
        var interest = $('#callback-id3');
        var label_phone = $('#label_callback-id1').parent();
        var label_term = $('#label_callback-id2').parent();
        var label_interest = $('#label_callback-id3').parent();

        var termControl = $('#callback-id2Control').val();

        var allFields = $([]).add(label_phone).add(label_term).add(label_interest);
        var bValid = true;
        var termValid = true;
        //alert(bValid);
        allFields.removeClass("fault");
        //alert(bValid);
        bValid = checkLength(phone, label_phone, 3, 80) && bValid;
        //alert(bValid);
        //if (!bValid) $('label#label_phone').css('color','red');
        termValid = checkLength(term, label_term, 3, 80);
        bValid = checkLength(term, label_term, 3, 80) && bValid;
        if (termControl === term.val()) {
            bValid = false;
            label_term.addClass("fault");
        } else if (!termValid) {
            term.val(termControl);
        }
        //alert(bValid);
        //if (!bValid) $('label#label_term').css('color','red');
        bValid = checkLength(interest, label_interest, 3, 800) && bValid;
        //alert(bValid);
        //if (!bValid) $('label#label_interest').css('color','red');
        //alert('test');
        bValid = checkRegexp(phone, /^(\+420)? ?[0-9]{3} ?[0-9]{3} ?[0-9]{3}$/i, label_phone) && bValid;
        //if (!bValid) $('label#label_phone').css('color','red');

        //alert(bValid);

        if (!bValid) {
            $('#we-call-back .message').hide();
            $('#we-call-back .message.fault').show();
            return false;
        } else {
            $('#we-call-back .message').hide();
            $('#we-call-back .message.success').show();
        }

    });


    // obsluhuje modaly
    $('button.ui-dialog-titlebar-close').attr('title', 'Zavřít okno');

    // obsluhuje zalozky a k nim prirazene karty
    if ($('div.bookmarks, div.cards').length) {
        $('div.bookmarks li:first-child').find('a').addClass('active');
        $('div.cards > div').not(':first-child').addClass('inactive');
    }

    $('div.bookmarks a').click(function() {
        $(this).parents('div.bookmarks').find('a').removeClass('active');
        $(this).addClass('active');
        var el = $(this).parents('li').attr("class");
        if ($('div.dealer').length) {
            $(this).parents('div.cards').find('> div').not('.bookmarks').removeClass('active').addClass('inactive');
            $(this).parents('div.cards').find('> div.' + el).removeClass('inactive').addClass('active');
        }
        else {
            $(this).parents('div.bookmarks').find('+ div.cards > div').removeClass('active').addClass('inactive');
            $(this).parents('div.bookmarks').find('+ div.cards > div').each(function(index) {
                if ($(this).hasClass(el)) {
                    $(this).removeClass('inactive').addClass('active');
                }
            });
        }
    });

    if ($('#getValue').length) {
        var getValue = $('#getValue').val();
        if (getValue) {
            $('div.bookmarks li.' + getValue + ' a').trigger('click');
            $('html,body').animate({
                scrollTop: $('div.container').children('div.bookmarks').offset().top
            }, 1000);
        }
    }


    /* STRANKOVANI */
    if ($('#pagerLimit').length) {
        var getValue = $('#pagerLimit').val();
        if (getValue) {
            $('html,body').animate({
                scrollTop: $('div.container div.articles article:eq(' + getValue + ')').offset().top
            }, 500);
        }
    }
    
    // dokresluje produktove menu level1 pri active
	$('header div.products.level1 li:first-child a.active').parent().addClass('active');
	$('header div.products.level1 li:last-child a.active').parent().addClass('active');
});
